import {getRepository} from 'typeorm'
import User from '../models/User'
import uploadConfig from '../config/upload'
import path from 'path'

import AppError from '../errors/AppErros'


import fs from 'fs'

interface Request{
  user_id:string
  avatarFilename:string
}

class UpdateUserAvatarService{
  public async execute({user_id,avatarFilename}:Request):Promise<User>{

    const usersRepository = getRepository(User)

    const user = await usersRepository.findOne(user_id)

    if(!user){
      throw new AppError('Only authenticated can change avatar',401)
    }

    if(user.avatar){
      //Deletar avatar anterior

      const userAvatarFilePath = path.join(uploadConfig.directory,user.avatar)
      const userAvatarFileExists = await fs.promises.stat(userAvatarFilePath)

      if(userAvatarFileExists){
        await fs.promises.unlink(userAvatarFilePath)
      }
    }

    user.avatar = avatarFilename

    await usersRepository.save(user)

    return user


  }
}

export  {UpdateUserAvatarService}
